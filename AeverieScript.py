from arango import ArangoClient
from ncclient import manager
from collections import OrderedDict
import os,sys, argparse
import xmltodict
import xml.dom.minidom
import json
import re, ast, time
import paramiko
import logging
from flask import Flask, request
from flask_restful import Resource, Api
from flask_socketio import SocketIO


app = Flask(__name__)
api = Api(app)
socketio = SocketIO(app)


class Main_steps():
    print "Network Mgnt System"
    def __init__(self, starting_ip_add, ending_ip_add, user, password, topo_name, meteor_userid):
        self.starting_ip_add = starting_ip_add
        self.ending_ip_add = ending_ip_add
        self.user = user
        self.password = password
        self.topo_key = topo_name + meteor_userid
        self.meteor_userid = meteor_userid
        self.netconf_instance_dict = {}
        self.topology_search_logs = []
            
    def get_ip_list(self):
        print self.ntw_range
        ip_list=[]
        ip1, ip2 = self.ntw_range.split('-')
        ip_add1 = ip1.split('/')[0].strip(" ")
        ip_list.append(ip_add1)
        ip_add2 = ip2.split('/')[0].strip(" ")
        ip_list.append(ip_add2)
        ip_list_appended = self.generate_ip_list(ip_add1,ip_add2)
        return ip_list_appended

# Generate all the ip addresses in the range
    def generate_ip_list(self, start_ip, end_ip):
        start = list(map(int, start_ip.split(".")))
        end = list(map(int, end_ip.split(".")))
        if start > end:
            swp=end
            end=start
            start=end

        temp = start
        ip_range = []

        ip_range.append(start_ip)
        while temp != end:
            start[3] += 1
            for i in (3, 2, 1):
                if temp[i] == 256:
                    temp[i] = 0
                    temp[i-1] += 1
            ip_range.append(".".join(map(str, temp)))
        return ip_range

#Check if hostname reachable by pinging
    def ping(self, host):
        import subprocess
        import platform

        # Ping parameters as function of OS
        ping_str = "-n 1" if platform.system().lower() == "windows" else "-c 1"
        args = "ping " + " " + ping_str + " " + host
        need_sh = False if platform.system().lower() == "windows" else True

        # Ping
        return subprocess.call(args, shell=need_sh) == 0

# Check for the server accessibility and create the respective vertex
    def socket_connection(self, hostname, ip_reachable, ip_unreachable):
        sshClient = paramiko.SSHClient()
        sshClient.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        #sshClient.load_system_host_keys()
        if self.ping(hostname)== True:
            try:
                connect = sshClient.connect(hostname, username=self.user, password=self.password)
                #add timeout argument in sshClient connect 10s
                print(hostname+' Reachable')
                socketio.emit('log', hostname+' Reachable'+ "@" + self.meteor_userid)
                self.topology_search_logs.append(hostname+' Reachable')
                self.interfaces.extend(self.netconfd_xml_json_vertex(hostname))
                #print self.interfaces
                ip_reachable.append(hostname)
                
            except Exception, e:
                print e
                ip_unreachable.append(hostname)
                if "connection attempt failed" in str(e):
                    print(hostname +' - Host not reachable')
                    socketio.emit('log', hostname +' - Host not reachable'+ "@" + self.meteor_userid)
                    self.topology_search_logs.append(hostname +' - Host not reachable')
                else:
                    print("Node creation for "+hostname+" not successful")
                    socketio.emit('log', "Node creation for "+hostname+" not successful"+ "@" + self.meteor_userid)
                    self.topology_search_logs.append("Node creation for "+hostname+" not successful")
        else:
            print(str(hostname)+" cannot be pinged!")
            socketio.emit("log", hostname+" cannot be pinged!"+ "@" + self.meteor_userid)
            self.topology_search_logs.append(hostname+" cannot be pinged!")

# Collect number of ips reachable
    def check_network_connectivity(self):
        ip_list = self.generate_ip_list(self.starting_ip_add, self.ending_ip_add)
        ip_reachable=[]
        ip_unreachable=[]
        self.interfaces = []
        self.range_reachable = []
        #print('ip_add=' + ip_add + ', port='+ port)
        for i in range(len(ip_list)):
            self.socket_connection(ip_list[i], ip_reachable, ip_unreachable)
        self.range_reachable = ip_reachable
        #print self.netconf_instance_dict
        print('vertex/node creation complete')
        socketio.emit('log', "vertex/node creation complete"+ "@" + self.meteor_userid)
        self.topology_search_logs.append("vertex/node creation complete")
        return self.range_reachable, self.interfaces


# Collecting data from the servers and convertion from xml to json
    def netconfd_xml_json_vertex(self, hostname):
        start = time.time()
        self.hostname = hostname
        interfaces=[]
        errors =[]
        ip = []
        json_final_vertex = open('lldp_nodes_details.json','w+')
        for i in range(1):
            #print hostname
            try:
                with manager.connect(host = hostname,
                                    username = self.user,
                                    password = self.password,
                                    hostkey_verify=False, 
                                    allow_agent=False,
                                    look_for_keys=False) as netconf_manager:
                    print netconf_manager
                    
                    self.netconf_instance_dict[hostname] = netconf_manager

                    filter_cmm = '''<cmmStackUnitTable></cmmStackUnitTable>'''
                    
                    data_cmm = netconf_manager.get(('subtree', filter_cmm))
                    #print data_cmm

                    xmlstr = xml.dom.minidom.parseString(str(data_cmm))
                    pretty_xml_as_string = xmlstr.toprettyxml()
                    json_cmm_data = xmltodict.parse(pretty_xml_as_string)
                    #print json_cmm_data
                    #print json.dumps(json_cmm_data)
                    #json_int_data = json.dumps(xml_json)
                    json_data_cmm = json_cmm_data['rpc-reply']['data']['cmmStackUnitTable']
                    
                    #print json_data_cmm

                    self.collect_node_details(json_data_cmm)

                    #filter_lldp = '''<vr xmlns="http://www.ipinfusion.com/CMLSchema/zebos"><interface></interface></vr>'''
                    filter_lldp = '''<vr><interface></interface></vr>'''
                    
                    data = netconf_manager.get(('subtree', filter_lldp))
                    

                    xmlstr = xml.dom.minidom.parseString(str(data))
                    pretty_xml_as_string = xmlstr.toprettyxml()
                    json_int_data = xmltodict.parse(pretty_xml_as_string)
                    #json_int_data = json.dumps(xml_json)
                    json_data = json_int_data['rpc-reply']['data']['vr']
                    
                    final, inter = self.collect_lldp_details(hostname, json_data)
                    
                    
                    interfaces.extend(inter)
                    final = json.dumps(final)
                    json_final_vertex.write(final)
                    json_final_vertex.write('\n')
                    end = time.time()
                    print end - start
                    return interfaces
                    
            except Exception, e:
                end = time.time()
                print end - start
                print "Host reachable but NetConf details processing not successful for host: "+ hostname
                socketio.emit('log', "Host reachable but NetConf details processing not successfull for host: "+ hostname+ "@" + self.meteor_userid)
                self.topology_search_logs.append("Host reachable but NetConf details processing not successfull for host: "+ hostname)
                errors.append(e)
                ip.append(hostname)
        
# Enable the following block for sample output data
                #json_data=[]
                #files = ['node_7003_json.json', 'node_7005_json.json', 'node_7046_json.json']
                
                #for fls in files:
                #    with open( fls ,'r') as f:
                #        datastore = json.load(f)
                #        json_data = datastore['rpc-reply']['data']['vr']
                #    final, inter = self.collect_lldp_details(hostname, json_data)
                #    interfaces.extend(inter)
                #    final = json.dumps(final)
                #    json_final_vertex.write(final)
                #    json_final_vertex.write('\n')

                #print errors
        return interfaces


    def occupied_ports(self, data, key) :
        #if data is not ordered list, remove outermost for loop
        #and replace _dict with data in _dict.items()
        for _dict in data:
            for k, v in _dict.items ():
                if isinstance (v, OrderedDict):
                    for found in self.occupied_ports (v, key):
                        yield found
                if k == key:
                    yield v

    def collect_node_details(self, data):
        #array to hold transindex from netconf details
        busy_ports=[]
        #print data
        for value in self.occupied_ports(data["cmmTransEEPROMTable"], "cmmTransIndex"):  
            busy_ports.append(int(value))
        #2D array for storing 1Gig,  10Gig, 25Gig, 40Gig,100Gig connected ports
        port_array=[[],[],[],[],[]]

        if self.topo.has_vertex_collection("nodes"):
            node = self.topo.vertex_collection("nodes")
        else:
            node = self.topo.create_vertex_collection("nodes")
			
        if node.has(self.hostname):
            node.delete(self.hostname)
        node.insert({'_key': self.hostname})
        node.update({'_key': self.hostname, 'ModelName' : data['cmmStackUnitModelName']})
        node.update({'_key': self.hostname, 'MAC' : data['cmmStackUnitMacAddress']})
        node.update({'_key': self.hostname, 'SwitchChipRev' : data['cmmStackUnitSwitchChipRev']})
        node.update({'_key': self.hostname, 'VendorName' : data['cmmStackVendorName']})
        node.update({'_key': self.hostname, 'OnieVersion' : data['cmmStackOnieVersion']})
        node.update({'_key': self.hostname, 'Platform' : data['cmmStackPlatformName']})
        node.update({'_key': self.hostname, 'Label_Revision' : data['cmmStackLabelRevision']})
        node.update({'_key': self.hostname, 'Serial_No' : data['cmmStackUnitSerialNumber']})
        for items in data['cmmTransEEPROMTable']:
            for k, v in items.items ():
                if(k=='cmmTransVendorPartNumber'):
                    node.update({'_key': self.hostname, 'Part_No' : v})


        node.update({'_key': self.hostname, 'Manufacturer' : data['cmmStackMfg']})
        node.update({'_key': self.hostname, 'Manufacture_Date' : data['cmmStackUnitMfgDate']})



        try:
            cmmGigPorts = [int(data['cmmStackUnitNum1GigEtherPorts']), int(data['cmmStackUnitNum1GigEtherPorts'])+int(data['cmmStackUnitNum10GigEtherPorts']), int(data['cmmStackUnitNum1GigEtherPorts'])+int(data['cmmStackUnitNum10GigEtherPorts'])+int(data['cmmStackUnitNum25GigEtherPorts']), int(data['cmmStackUnitNum1GigEtherPorts'])+int(data['cmmStackUnitNum10GigEtherPorts'])+int(data['cmmStackUnitNum25GigEtherPorts'])+int(data['cmmStackUnitNum40GigEtherPorts']),
                          int(data['cmmStackUnitNum1GigEtherPorts'])+int(data['cmmStackUnitNum10GigEtherPorts'])+int(data['cmmStackUnitNum25GigEtherPorts'])+int(data['cmmStackUnitNum40GigEtherPorts'])+int(data['cmmStackUnitNum100GigEtherPorts'])]
            node.update({'_key': self.hostname, 'Noof1G': int(data['cmmStackUnitNum1GigEtherPorts'])})
            
        except Exception, e:
            cmmGigPorts = [0, int(data['cmmStackUnitNum10GigEtherPorts']), int(data['cmmStackUnitNum10GigEtherPorts'])+int(data['cmmStackUnitNum25GigEtherPorts']), int(data['cmmStackUnitNum10GigEtherPorts'])+int(data['cmmStackUnitNum25GigEtherPorts'])+int(data['cmmStackUnitNum40GigEtherPorts']),
                            int(data['cmmStackUnitNum10GigEtherPorts'])+int(data['cmmStackUnitNum25GigEtherPorts'])+int(data['cmmStackUnitNum40GigEtherPorts'])+int(data['cmmStackUnitNum100GigEtherPorts'])]
            node.update({'_key': self.hostname, 'Noof1G': 0})
        
        node.update({'_key': self.hostname, 'Noof10G' : int(data['cmmStackUnitNum10GigEtherPorts'])})
        node.update({'_key': self.hostname, 'Noof25G' : int(data['cmmStackUnitNum25GigEtherPorts'])})
        node.update({'_key': self.hostname, 'Noof40G' : int(data['cmmStackUnitNum40GigEtherPorts'])})
        # node.update({'_key': self.hostname, 'Noof100G' : int(data['cmmStackUnitNum100GigEtherPorts'])})
        node.update({'_key': self.hostname, 'Noof100G' : 32})
        node.update({'_key': self.hostname, 'TopologyKey' : self.topo_key})


        x=0
        for i in range(len(cmmGigPorts)):
            if cmmGigPorts[i]-x!= 0:
                for j in range(len(busy_ports)):
                    if busy_ports[j]<=cmmGigPorts[i]:
                        port_array[i].append(busy_ports[j]-x)
                        if(j+1==len(busy_ports)):
                            busy_ports=busy_ports[j+1:]
                            break
                    else:
                        busy_ports=busy_ports[j:]
                        break
                x=cmmGigPorts[i]

        #populating node document with respective connected ports
        for i in range(len(port_array)):
            if len(port_array[i])!=0:
                if(i==0):
                    node.update({'_key': self.hostname, 'Connected1G' : port_array[i]})
                elif i==1:
                    node.update({'_key': self.hostname, 'Connected10G' : port_array[i]})
                elif i==2:
                    node.update({'_key': self.hostname, 'Connected25G' : port_array[i]})
                elif i==3:
                    node.update({'_key': self.hostname, 'Connected40G' : port_array[i]})
                else:
                    node.update({'_key': self.hostname, 'Connected100G' : port_array[i]})

# Collect the lldp data from json and create vertex for ArangoDB Graph database
    def collect_lldp_details(self, host, data):
        try:
            mip = {}
            node_coll = []
            final = []
            interface = []
            #key = host

            if self.topo.has_vertex_collection("nodes"):
                node = self.topo.vertex_collection("nodes")
            else:
                node = self.topo.create_vertex_collection("nodes")

            for item in data:
                if str(item) == 'interface':
                    list_interface = []
                    inter_details = {}
                    for i in range(len(data['interface'])):
                        for inter in data['interface'][i]:
                            if inter == 'ipAddr' and "lo" not in data['interface'][i]['ifName']:
                                mgmt_ip = str(data['interface'][i]['ipAddr'])
                                #print str(data['interface'][i]['ipAddr'])
                                inter_details['mgmt_ip'] = mgmt_ip
                                #key = mgmt_ip.split('/')[0]
                                #if node.has(key):
                                #    node.delete(key)
                                #node.insert({'_key': key})
                                #print key
                    node.update({'_key': self.hostname, 'mgmt_ip' : mgmt_ip})
                    #print data
                    for i in range(len(data['interface'])):
                        for inter in data['interface'][i]:
                            if inter == 'lldpv2Interface':
                                source_port = str(data['interface'][i]['ifName'])
                                #print source_port
                                ref = self.hostname+'_'+source_port
                                #print ref
                            
                                inter_details['source_port_disp'] = source_port
                                list_interface.append(source_port)
                            
                                for val in data['interface'][i]['lldpv2Interface']['lldpAgent']:
                                    d = data['interface'][i]['lldpv2Interface']['lldpAgent']
                                    if str(val) == 'destMacAddr':
                                        mac = str(d['destMacAddr'])
                                    #print mac
                                    #inter_details['mac'] = mac
                                    if str(val) == 'lldpRxSm':
                                        inter_details['ref'] = ref
                                        ip = str(d['lldpRxSm']['rlldpList']['remoteChassisId'])
                                        inter_details['remote_ip']= ip
                                        #inter_details['key'] = key+'_'+source_port
                                        target_port = str(d['lldpRxSm']['rlldpList']['remotePortDescrip'])
                                        inter_details['target_port_disp']= target_port                      
                                        mac = str(d['lldpRxSm']['rlldpList']['remoteMgmtInfo']['remoteMgmtAddr'])
                                        inter_details['dest_mac'] = mac
                                        remoteSysName = str(d['lldpRxSm']['rlldpList']['remoteSysName'])
                                        remoteSysDescr = str(d['lldpRxSm']['rlldpList']['remoteSysDescr'])
                                        inter_details['remoteSysName'] = remoteSysName
                                        inter_details['remoteSysDescr'] = remoteSysDescr
                                    inter_details['knmp_on'] = False
                                    inter_details['snmp_on'] = True
                                    inter_details['vlan'] = 1
                                    inter_details['flag']= 0
                                    inter_details['netmask'] = "255.255.255.0"
                                    inter_details['device_name'] = "device_2"
                                    inter_details['ip_on']= False
                                    inter_details['key_id']= self.hostname
                                    inter_details['gateway'] ="0.0.0.0"
                                
                                    if mgmt_ip not in mip:
                                        mip['node_'+host] = mgmt_ip
                                #print type(node.get(key))
                                #print inter_details
                                interface.append(str(inter_details))
                                node_coll.append(node.get(self.hostname))
                    node.update({'_key': self.hostname, 'interface' :str(list_interface)})
                    final = node.get(self.hostname)
                    #del_entry[node] = self.hostname
                    
            return final, interface

        except Exception, e:
            print e
            print("Vertex/Node creation not complete")
            socketio.emit('log', "Vertex/Node creation not complete"+ "@" + self.meteor_userid)
            self.topology_search_logs.append("Vertex/Node creation not complete")

# Create the links/edges for ArangoDB Graph database
    def netconfd_xml_json_edges(self, interfaces):
        json_final_edges = open('lldp_links_details.json','w+')
        edge_list = []
        node_coll = interfaces
        for entry in node_coll:
            entry = ast.literal_eval(entry)
            #print entry
            for ent in node_coll:
                ent = ast.literal_eval(ent)
                #print ent
                if entry['remote_ip'] in ent['mgmt_ip'] and ent['remote_ip'] in entry['mgmt_ip'] and entry['source_port_disp'] == ent['target_port_disp']:
                    #print True
                    frm = "nodes/"+entry['key_id']
                    to = "nodes/"+ent['key_id']
                    f = []
                    t = []
                    f.append(str(entry['key_id']))
                    t.append('int_'+ str(ent['key_id']))
                    key_id = re.sub('[.]', '', str(entry['key_id']))
                    links = str(entry['source_port_disp'])+ key_id
                    key = links
                    #print topo.edge_definitions()
                    
                    #topo.delete_edge_definition( key_id , purge=True)
                    if self.topo.has_edge_definition(links):
                        links = self.topo.edge_collection(links)
                    else:
                        links = self.topo.create_edge_definition(
                            edge_collection=links,
                            from_vertex_collections= ['from'] ,
                            to_vertex_collections= ['to']
                            )
                    if links.has(key):
                        links.delete(key)
                    links.insert({
                        '_key': key,
                        '_from': frm,
                        '_to': to
                        })
                    links.update({
                        '_key': key,
                        'protocol' : 'lldp',
                        'link_type' : 'NA',
                        'source_port_disp' : entry['source_port_disp'],
                        'remote_ip' : entry['remote_ip'],
                        'target_port_disp': entry['target_port_disp'],
                        'dest_mac' : entry['dest_mac'],
                        'remoteSysName' : entry['remoteSysName'],
                        'remoteSysDescr' : entry['remoteSysDescr'],
                        'knmp_on' : entry['knmp_on'],
                        'snmp_on' : entry['snmp_on'],
                        'vlan' : 1,
                        'flag': 0,
                        'netmask' : entry['netmask'],
                        'ip_on': entry['ip_on'],
                        'gateway' :entry['gateway'],
			'TopologyKey': self.topo_key
                        })
                    #print links.has(key)
                    #final_edge = []
                    final_edge = links.get(key)
                    final_edge = json.dumps(final_edge)
                    json_final_edges.write(final_edge)
                    json_final_edges.write('\n')
                    #links.delete(key)

                    edge_list.append(self.topo.edge_definitions())
                    #self.topo.delete_edge_definition( key_id , purge=True)
        print 'edge/links creation complete'
        socketio.emit('log', "edge/links creation complete"+ "@" + self.meteor_userid)
        self.topology_search_logs.append("edge/links creation complete")


# Starting ArangoDB client
    def create_ArangoDb_client(self):
        try:
            client = ArangoClient(protocol='http', host='172.26.1.22', port=8529)
            #client = ArangoClient(protocol='http', host='10.10.26.172', port=8529)
            sys_db = client.db('_system', username='root', password='root123')
            if not sys_db.has_database('Topology_Discovery'):
                db = sys_db.create_database('Topology_Discovery')
            else:
                db = client.db('Topology_Discovery', username='root', password='root123')
            #print db.graphs()
            self.db = db

            if db.has_graph('topology'):
                topo = db.graph('topology')
            else:
                topo = db.create_graph('topology')
            self.topo = topo
            #print topo
            #return db, topo
        except Exception, e:
            print("Arangodb client no started. Please start the client and re-trigger!!!")
            socketio.emit('log', "Arangodb client no started. Please start the client and re-trigger!!!"+ "@" + self.meteor_userid)
            self.topology_search_logs.append("Arangodb client no started. Please start the client and re-trigger!!!")
            sys.exit()
            
         
def main(starting_ip_add, ending_ip_add, user, password, topo_name,meteor_userid):
    start_topo = Main_steps(starting_ip_add, ending_ip_add, user, password, topo_name,meteor_userid)
    start_topo.create_ArangoDb_client()
    #node.get_ip_list()
    socketio.emit('log', "Checking connectivity" + "@" + start_topo.meteor_userid)
    start_topo.topology_search_logs.append("Checking connectivity")
    print "Checking connectivity"  
    range_reachable, interface = start_topo.check_network_connectivity()
    client = ArangoClient(protocol='http', host='172.26.1.22', port=8529)
    #client = ArangoClient(protocol='http', host='10.10.26.172', port=8529)
    db = client.db('Topology_Discovery', username='root', password='root123')
    if len(range_reachable) == 0:
        start_topo.topology_search_logs.append("All Servers Not Reachable in the provided range!!!")
        db.aql.execute('FOR doc IN Topology_Configuration'
                                ' FILTER doc._key == @topology_key'
                                ' UPDATE doc WITH { DiscoveryStatus: "Failed", "SearchLogs": @logs  } IN Topology_Configuration',
                                bind_vars={'topology_key': start_topo.topo_key, 'logs': start_topo.topology_search_logs})
        print('All Servers Not Reachable in the provided range!!!')
        socketio.emit('log', "All Servers Not Reachable in the provided range!!!"+ "@" + start_topo.meteor_userid)
        sys.exit()
    else:
        start_topo.topology_search_logs.append("Reachable servers:")
        socketio.emit('log', "Reachable servers:"+ "@" + start_topo.meteor_userid)
        for i in range(len(range_reachable)):
            start_topo.topology_search_logs.append(range_reachable[i])
            socketio.emit('log', range_reachable[i]+ "@" + start_topo.meteor_userid)
            
        db.aql.execute('FOR doc IN Topology_Configuration'
                       ' FILTER doc._key == @topology_key'
                       ' UPDATE doc WITH { DiscoveryStatus: "Completed", "SearchLogs": @logs } IN Topology_Configuration',
                       bind_vars={'topology_key': start_topo.topo_key, 'logs': start_topo.topology_search_logs})
        print("Reachable servers:")
        print range_reachable
    
    #interface = node.netconfd_xml_json_vertex(range_reachable)
    start_topo.netconfd_xml_json_edges(interface)
    print "Done"
    return 0


class runNMScript(Resource):
    def get(self, first_ip_add, second_ip_add, username, password,topo_name,meteor_userid):
        result = main(first_ip_add, second_ip_add, username, password, topo_name,meteor_userid)

        if result == 0:
            return "Script execution successful"

    
api.add_resource(runNMScript, '/<first_ip_add>/<second_ip_add>/<username>/<password>/<topo_name>/<meteor_userid>')




if __name__ == '__main__':

    socketio.run(app, host='172.26.1.27', port = 3002)
    #socketio.run(app, host='localhost', port = 3002)
