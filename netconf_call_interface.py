from ncclient import manager
import xmltodict
import xml.dom.minidom
import json
from flask import Flask
from flask_restful import Resource, Api
import re


app = Flask(__name__)
api = Api(app)


def netconfd_interface_optical_json(hostname, interface, platform, username, password):
    for i in range(1):
        try:
            with manager.connect(host=hostname,
                                 username=username,
                                 password=password,
                                 hostkey_verify=False,
                                 allow_agent=False,
                                 look_for_keys=False) as netconf_manager:
                #print netconf_manager
                slotIndexArr = [1, 2, 3, 4, 5, 6, 7, 8]
                platform = platform.split("_")
                platform = platform[len(platform)-1].split("-")
                slotIndex = -1
                optical_json_data = None

                if "sn1" in platform[0]:
                    interface = interface
                elif "xc" in platform[0]:
                    num = re.findall('\d+', interface)
                    num = map(int, num)
                    if(num[0] > 16):
                        num = num[0]-16
                        slotIndex = (num-1)/2
                        interface = "oe"+str(num)
                    else:
                        interface = interface+"/1"
                elif "x" in platform[0]:
                    interface = interface+"/1"



                filter_cmm = '''<cmmStackUnitTable></cmmStackUnitTable>''' 
                data_cmm = netconf_manager.get(('subtree', filter_cmm))
                #print data_cmm
                xmlstr = xml.dom.minidom.parseString(str(data_cmm))
                pretty_xml_as_string = xmlstr.toprettyxml()
                json_cmm_data = xmltodict.parse(pretty_xml_as_string)
                #print json_cmm_data
                cmm_data = json_cmm_data['rpc-reply']['data']['cmmStackUnitTable']
    

                #print interface
                filter_interface = '''<vr><interface><ifName>''' + \
                    interface + '''</ifName></interface></vr>'''
                interfaceData = netconf_manager.get(
                    ('subtree', filter_interface))
                # print interfaceData
                xmlstr = xml.dom.minidom.parseString(str(interfaceData))
                pretty_xml_as_string = xmlstr.toprettyxml()
                json_int_data = xmltodict.parse(pretty_xml_as_string)
                #json_int_data = json.dumps(xml_json)
                interface_json_data = json_int_data['rpc-reply']['data']['vr']

                if (slotIndex >= 0) and (interface.startswith("o")):
                    filter_optical = '<cmmOpticalControllerTable><slotIndex>' + \
                        str(slotIndexArr[slotIndex]) + \
                        '</slotIndex></cmmOpticalControllerTable>'
                    opticalData = netconf_manager.get(
                        ('subtree', filter_optical))
                    # print opticalData
                    xmlstr = xml.dom.minidom.parseString(str(opticalData))
                    pretty_xml_as_string = xmlstr.toprettyxml()
                    json_int_data = xmltodict.parse(pretty_xml_as_string)
                    #json_int_data = json.dumps(xml_json)
                    optical_json_data = json_int_data['rpc-reply']['data']

                #print optical_json_data
                return interface_json_data, optical_json_data,cmm_data

        except Exception, e:
            print "Error occured"
            print e


def netconfd_component_json(hostname,username,password,component):
    for i in range(1):
        try:
            with manager.connect(host=hostname,
                                 username=username,
                                 password=password,
                                 hostkey_verify=False,
                                 allow_agent=False,
                                 look_for_keys=False) as netconf_manager:
                #print netconf_manager'
                if component == "fan":
                    fan_info = []
                    filter_cmm = '''<cmmStackUnitTable><cmmFanTrayTable></cmmFanTrayTable></cmmStackUnitTable>''' 
                    data_cmm = netconf_manager.get(('subtree', filter_cmm))
                    #print data_cmm
                    xmlstr = xml.dom.minidom.parseString(str(data_cmm))
                    pretty_xml_as_string = xmlstr.toprettyxml()
                    json_cmm_data = xmltodict.parse(pretty_xml_as_string)
                    #print json_cmm_data
                    if 'cmmFanTrayTable' in json_cmm_data['rpc-reply']['data']['cmmStackUnitTable'].keys(): 
                        cmm_fanTable_data = json_cmm_data['rpc-reply']['data']['cmmStackUnitTable']['cmmFanTrayTable']
                        fan_info.append(cmm_fanTable_data)
                    return fan_info
                
                if component == "temperature":
                    temp_info = []
                    filter_cmm = '''<cmmStackUnitTable><cmmSysTemperatureTable></cmmSysTemperatureTable></cmmStackUnitTable>''' 
                    data_cmm = netconf_manager.get(('subtree', filter_cmm))
                    #print data_cmm
                    xmlstr = xml.dom.minidom.parseString(str(data_cmm))
                    pretty_xml_as_string = xmlstr.toprettyxml()
                    json_cmm_data = xmltodict.parse(pretty_xml_as_string)
                    #print json_cmm_data
                    if 'cmmSysTemperatureTable' in json_cmm_data['rpc-reply']['data']['cmmStackUnitTable'].keys(): 
                        cmm_temperatureTable_data = json_cmm_data['rpc-reply']['data']['cmmStackUnitTable']['cmmSysTemperatureTable']
                        temp_info.append(cmm_temperatureTable_data)

                    #SWITCH_TEMP_TABLE
                    filter_cmm = '''<cmmStackUnitTable><cmmSwitchTemperatureTable></cmmSwitchTemperatureTable></cmmStackUnitTable>''' 
                    data_cmm = netconf_manager.get(('subtree', filter_cmm))
                    xmlstr = xml.dom.minidom.parseString(str(data_cmm))
                    pretty_xml_as_string = xmlstr.toprettyxml()
                    json_cmm_data = xmltodict.parse(pretty_xml_as_string)
                    if 'cmmSwitchTemperatureTable' in json_cmm_data['rpc-reply']['data']['cmmStackUnitTable'].keys(): 
                        cmm_switch_temperature_data = json_cmm_data['rpc-reply']['data']['cmmStackUnitTable']['cmmSwitchTemperatureTable']
                        temp_info.append(cmm_switch_temperature_data)
                    return  temp_info
                
                if component == "psu":
                    psu_info = []
                    filter_cmm = '''<cmmStackUnitTable><cmmSysPowerSupplyTable></cmmSysPowerSupplyTable></cmmStackUnitTable>''' 
                    data_cmm = netconf_manager.get(('subtree', filter_cmm))
                    #print data_cmm
                    xmlstr = xml.dom.minidom.parseString(str(data_cmm))
                    pretty_xml_as_string = xmlstr.toprettyxml()
                    json_cmm_data = xmltodict.parse(pretty_xml_as_string)
                    #print json_cmm_data
                    if 'cmmSysPowerSupplyTable' in json_cmm_data['rpc-reply']['data']['cmmStackUnitTable'].keys(): 
                        cmm_psuTable_data = json_cmm_data['rpc-reply']['data']['cmmStackUnitTable']['cmmSysPowerSupplyTable']
                        psu_info.append(cmm_psuTable_data)
                        
                    
                    #Component Status data
                    filter_cmm = '''<cmmStackUnitTable><cmmSysComponentStatusTable></cmmSysComponentStatusTable></cmmStackUnitTable>''' 
                    data_cmm = netconf_manager.get(('subtree', filter_cmm))
                    #print data_cmm
                    xmlstr = xml.dom.minidom.parseString(str(data_cmm))
                    pretty_xml_as_string = xmlstr.toprettyxml()
                    json_cmm_data = xmltodict.parse(pretty_xml_as_string)
                    #print json_cmm_data
                    if 'cmmSysComponentStatusTable' in json_cmm_data['rpc-reply']['data']['cmmStackUnitTable'].keys(): 
                        cmm_psuTable_componentStatus_data = json_cmm_data['rpc-reply']['data']['cmmStackUnitTable']['cmmSysComponentStatusTable']
                        psu_info.append(cmm_psuTable_componentStatus_data)
                        
                    return psu_info

        except Exception, e:
            print "Error occured"
            print e


class runInterfaceDef(Resource):
    def get(self, hostname, interface, platform, username, password):
        result = None
        interface_result, optical_info, cmm_info = netconfd_interface_optical_json(
            hostname, interface, platform, username, password)
        if optical_info is None:
            result = [interface_result,"",cmm_info]
        else:
            result = [interface_result, optical_info,cmm_info]
        return result

class runComponentDef(Resource):
    def get(self, hostname, username, password, component):
            component_result = netconfd_component_json(
                hostname, username, password, component)
            return component_result




api.add_resource(
    runInterfaceDef, '/<hostname>/<interface>/<platform>/<username>/<password>')

api.add_resource(
    runComponentDef, '/<hostname>/<username>/<password>/<component>')


if __name__ == '__main__':

    app.run(host='172.26.1.27', port=3003)
    #app.run(host='localhost', port=3003)
